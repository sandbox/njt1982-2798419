<?php /**
 * @file
 * Contains \Drupal\page_title_formatter\Plugin\Field\FieldFormatter\PageTitleFormatter.
 */

namespace Drupal\page_title_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
// use Drupal\Component\Utility\Html;


/**
 * @FieldFormatter(
 *  id = "page_title",
 *  label = @Translation("Page Title"),
 *   field_types = {
 *     "string",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class PageTitleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#attached' => [
          'html_head' => [
            [
              [
                '#type' => 'html_tag',
                '#tag' => 'title',
                '#value' => $item->value,
              ],
              'title'
            ]
          ]
        ],
        '#markup' => '',
      ];
    }

    return $elements;
  }
}
